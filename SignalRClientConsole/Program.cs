﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalRClientConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            // Connects to the server hub
            IHubProxy _hub;
            string url = @"http://localhost:8080/";
            var connection = new HubConnection(url);
            _hub = connection.CreateHubProxy("ServerHub");
            _hub.On("ReceiveMessage", x =>
            ReceiveMessage(x));

            connection.Start().Wait();

            // Sends a message to the server method "DetermineLength"
            string line = "";
            line = Console.ReadLine();
            _hub.Invoke("DetermineLength", line).Wait();

            
            Console.ReadLine();
        }

        private static void ReceiveMessage(string newMessage)
        {
            Console.WriteLine(newMessage);
        }
    }
}

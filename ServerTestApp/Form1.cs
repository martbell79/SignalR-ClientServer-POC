﻿using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ServerTestApp
{
    public partial class Form1 : Form
    {
        ServerHub _hub;

        public Form1()
        {
            InitializeComponent();
            _hub = new ServerHub();

            string url = @"http://localhost:8080/";
            WebApp.Start<Startup>(url);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void btnBroadcastCustom_Click(object sender, EventArgs e)
        {
            _hub.BroadcastMessageToAll(txtBroadcastCustom.Text);
        }
    }
}

﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerTestApp
{
    public class ServerHub:Hub
    {
        public void BroadcastMessageToAll(string message)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<ServerHub>();
            //context.Clients.All.Send("Admin", "stop the chat");
            context.Clients.All.ReceiveMessage(message);
            //Clients.All.ReceiveMessage(message);
        }
    }
}

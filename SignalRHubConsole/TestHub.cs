﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalRHubConsole
{
    public class TestHub:Hub
    {
        // Receive message from client and print to server console
        public void DetermineLength(string message)
        {
            Console.WriteLine(message);
            string newMessage = "Hello All";
            Clients.All.ReceiveMessage(newMessage);
            
        }
    }
}
